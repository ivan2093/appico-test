import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }

  public async getUsers() {
    // The construction of the baseurl should go at the interceptor
    return this.http.get(environment.baseUrl + 'users').toPromise();
  }
}
