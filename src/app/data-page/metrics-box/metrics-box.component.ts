import { Component, OnInit, Input } from '@angular/core';
import { Label, MultiDataSet, Colors } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-metrics-box',
  templateUrl: './metrics-box.component.html',
  styleUrls: ['./metrics-box.component.sass']
})
export class MetricsBoxComponent implements OnInit {
  @Input() title: string;
  @Input() dataBars: any;
  @Input() totalData: number;
  @Input() color: string;
  doughnutChartLabels: Label[] = ['Completed', 'pending'];
  public blueColors = [
    {
      backgroundColor: ['#0077ff','#b3d6ff'],
    }
  ];
  public pinkColors = [
    {
      backgroundColor: ['#f0166d', '#f78ab6'],
    }
  ];
  doughnutChartData: MultiDataSet = [
    [55, 100]
  ];
  doughnutChartType: ChartType = 'doughnut';
  public chartOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: false,
    }
  };
  constructor() { }

  ngOnInit() {
  }

}
