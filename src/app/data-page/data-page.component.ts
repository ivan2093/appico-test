import { Component, OnInit } from '@angular/core';
import { UsersService } from '../providers/users/users.service';
import { ResultsService } from '../providers/results/results.service';

@Component({
  selector: 'app-data-page',
  templateUrl: './data-page.component.html',
  styleUrls: ['./data-page.component.sass']
})
export class DataPageComponent implements OnInit {
  arrayDataVarsTest: any = [
    {
    title: "Term 1",
    data: "85.58"
    },
    {
    title: "Term 2",
    data: "31.58"
    },
    {
    title: "Term 3",
    data: "53.42"
    },
    {
    title: "Term 4",
    data: "75.11"
  }];
  users: any = [];
  results: any = [];
 
  constructor(
    private usersService: UsersService,
    private resultsService: ResultsService
  ) { }

  ngOnInit() {
    this.getUsers();
    this.getResults();
  }

  private async getUsers() {
    this.users = await this.usersService.getUsers();
  }

  private async getResults() {
    this.results = await this.resultsService.getResults();
  }

}
