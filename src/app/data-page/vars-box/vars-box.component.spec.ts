import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarsBoxComponent } from './vars-box.component';

describe('VarsBoxComponent', () => {
  let component: VarsBoxComponent;
  let fixture: ComponentFixture<VarsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
