import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pending-bar',
  templateUrl: './pending-bar.component.html',
  styleUrls: ['./pending-bar.component.sass']
})
export class PendingBarComponent implements OnInit {
  @Input() completedCount: number;
  @Input() totalCount: number;

  constructor() { }

  ngOnInit() {
  }

}
