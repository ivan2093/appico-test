import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-box',
  templateUrl: './table-box.component.html',
  styleUrls: ['./table-box.component.sass']
})
export class TableBoxComponent implements OnInit {
  @Input() tableData: any;
  constructor() { }

  ngOnInit() {
  }

}
