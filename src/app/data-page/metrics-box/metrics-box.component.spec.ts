import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsBoxComponent } from './metrics-box.component';

describe('MetricsBoxComponent', () => {
  let component: MetricsBoxComponent;
  let fixture: ComponentFixture<MetricsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
