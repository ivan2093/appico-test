import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-row',
  templateUrl: './table-row.component.html',
  styleUrls: ['./table-row.component.sass']
})
export class TableRowComponent implements OnInit {
  @Input() row: any;
  @Input() index: number;
  constructor() { }

  ngOnInit() {
    if (this.index === 0) {
      this.row = Object.keys(this.row);
    } else {
      this.row = Object.values(this.row);
    }
    
  }

}
