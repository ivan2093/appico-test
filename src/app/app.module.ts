import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataPageComponent } from './data-page/data-page.component';
import { TopBarComponent } from './shared/top-bar/top-bar.component';
import { SideBarComponent } from './shared/side-bar/side-bar.component';
import { PendingBarComponent } from './data-page/pending-bar/pending-bar.component';
import { MetricsBoxComponent } from './data-page/metrics-box/metrics-box.component';
import { VarsBoxComponent } from './data-page/vars-box/vars-box.component';
import { TableBoxComponent } from './shared/table-box/table-box.component';
import { TableRowComponent } from './shared/table-box/table-row/table-row.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    DataPageComponent,
    TopBarComponent,
    SideBarComponent,
    PendingBarComponent,
    MetricsBoxComponent,
    VarsBoxComponent,
    TableBoxComponent,
    TableRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InlineSVGModule.forRoot(),
    HttpClientModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
