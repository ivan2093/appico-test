import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingBarComponent } from './pending-bar.component';

describe('PendingBarComponent', () => {
  let component: PendingBarComponent;
  let fixture: ComponentFixture<PendingBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
