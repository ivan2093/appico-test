import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataPageComponent } from './data-page/data-page.component';


const routes: Routes = [{
  path: 'dashboard/data',
  component: DataPageComponent
}, {
  path: '',
  redirectTo: 'dashboard/data',
  pathMatch: 'full'
}, {
  path: '',
  redirectTo: 'dashboard/data',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
