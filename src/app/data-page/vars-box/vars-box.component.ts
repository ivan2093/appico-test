import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vars-box',
  templateUrl: './vars-box.component.html',
  styleUrls: ['./vars-box.component.sass']
})
export class VarsBoxComponent implements OnInit {
  @Input() arrayData: any;
  constructor() { }

  ngOnInit() {
  }

}
