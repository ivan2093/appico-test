import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor(
    private http: HttpClient
  ) { }

  public async getResults() {
    return this.http.get(environment.baseUrl + 'results').toPromise();
  }
}
